# ds18b20_dts
This project reads the temperature data from all available ds18b20 probes using the one wire protocol
and sends them over the [Domain Type System (DTS)](https://gitlab.com/agates/domain-type-system).
