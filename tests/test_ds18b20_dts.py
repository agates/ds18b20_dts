# -*- coding: utf-8 -*-

#    ds18b20_dts
#    Copyright (C) 2018  Alecks Gates
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
import asyncio
from datetime import datetime
from io import StringIO
import math
from unittest.mock import MagicMock, Mock
import unittest.mock

from domaintypesystem import DomainTypeSystem
import pytest

from ds18b20_dts import ds18b20_dts


class AsyncMock(MagicMock):
    async def __call__(self, *args, **kwargs):
        return super(AsyncMock, self).__call__(*args, **kwargs)


def test_current_timestamp(mocker):
    mock = Mock(return_value=datetime(1970, 1, 1, 0, 0, 0, 0))

    class FakeDatetime:
        utcnow = mock

    mocker.patch.object(ds18b20_dts, 'datetime', new_callable=FakeDatetime)
    timestamp = ds18b20_dts.current_timestamp()

    mock.assert_called_once_with()
    pytest.approx(timestamp, 0)


def test_current_timestamp_nanoseconds(mocker):
    mocker.patch.object(ds18b20_dts, 'current_timestamp', return_value=1.0)

    timestamp = ds18b20_dts.current_timestamp_nanoseconds()

    # noinspection PyUnresolvedReferences
    ds18b20_dts.current_timestamp.assert_called_once_with()
    pytest.approx(timestamp, 1e9)


def test_read_rom(mocker):
    mock_name = "foo"

    mock_open = mocker.patch.object(ds18b20_dts, 'open')
    mock_open.return_value.__enter__.return_value = StringIO(mock_name)

    name = ds18b20_dts.read_rom("/folder")
    assert name == mock_name


@pytest.mark.asyncio
async def test_read_device(mocker):
    mock_lines = ["foo", "bar"]

    class AsyncStringIO(StringIO):
        async def read(self, **kwargs):
            return super(AsyncStringIO, self).read(**kwargs)

        async def __aenter__(self):
            return self

        async def __aexit__(self, exc_type, exc_val, exc_tb):
            pass

    mocker.patch.object(ds18b20_dts.aiofiles, 'open', return_value=AsyncStringIO('\n'.join(mock_lines)))

    name = await ds18b20_dts.read_device("/folder")
    assert name == mock_lines


@pytest.mark.asyncio
async def test_read_temp(mocker):
    mock_temperature = "55.5"
    mock_temperature_str = str(math.floor(float(mock_temperature) * 1000))
    mock_lines = ("YES", f"t={mock_temperature_str}")

    class AsyncReadDevice(MagicMock):
        async def __call__(self, *args, **kwargs):
            super(AsyncReadDevice, self).__call__(*args, **kwargs)
            return mock_lines

    mocker.patch.object(ds18b20_dts, 'read_device', new_callable=AsyncReadDevice)

    temperature = await ds18b20_dts.read_temp(None)

    assert temperature == mock_temperature


@pytest.mark.asyncio
async def test_read_temp_retry(mocker):
    mock_temperature = "55.5"
    mock_temperature_str = str(math.floor(float(mock_temperature) * 1000))
    mock_lines = (("NO", None), ("YES", f"t={mock_temperature_str}"))

    class AsyncReadDevice(MagicMock):
        def __init__(self, *args, **kwargs):
            super(AsyncReadDevice, self).__init__(*args, **kwargs)
            self.line_index = 0

        async def __call__(self, *args, **kwargs):
            super(AsyncReadDevice, self).__call__(*args, **kwargs)
            lines = mock_lines[self.line_index]
            self.line_index += 1
            return lines

    mocker.patch.object(ds18b20_dts, 'read_device', new_callable=AsyncReadDevice)

    temperature = await ds18b20_dts.read_temp(None)

    assert temperature == mock_temperature


@pytest.mark.asyncio
async def test_read_temp_error(mocker):
    mock_lines = ("YES", "foo")

    class AsyncReadDevice(MagicMock):
        async def __call__(self, *args, **kwargs):
            super(AsyncReadDevice, self).__call__(*args, **kwargs)
            return mock_lines

    mocker.patch.object(ds18b20_dts, 'read_device', new_callable=AsyncReadDevice)

    with pytest.raises(ValueError):
        await ds18b20_dts.read_temp(None)


@pytest.mark.asyncio
async def test_send_temperature(mocker, event_loop):
    dts = mocker.Mock()
    dts_send_struct_stub = mocker.stub(name='dts_send_struct_stub')

    # Need a stub wrapper since the provided stub isn't async
    async def stub_wrapper(foo):
        return dts_send_struct_stub(foo)

    dts.send_struct = stub_wrapper
    mock_temperature = "20.0"
    mock_current_timestamp_nanoseconds = 1.5
    mock_serial_number = "foo"
    mocker.patch.object(ds18b20_dts, 'current_timestamp_nanoseconds', return_value=mock_current_timestamp_nanoseconds)
    mock_temperature_event = {'temperature': mock_temperature,
                              'timestmap': int(mock_current_timestamp_nanoseconds),
                              'serial_number': mock_serial_number}
    mocker.patch.object(ds18b20_dts, 'TemperatureEvent', return_value=mock_temperature_event)

    await ds18b20_dts.send_temperature(dts, mock_serial_number, mock_temperature)

    # noinspection PyUnresolvedReferences
    dts_send_struct_stub.assert_called_once_with(mock_temperature_event)
    ds18b20_dts.TemperatureEvent.assert_called_once_with(temperature=bytes(str(mock_temperature), 'UTF-8'),
                                                         timestamp=int(mock_current_timestamp_nanoseconds),
                                                         model=b'DS18B20',
                                                         serial_number=mock_serial_number)


@pytest.mark.asyncio
async def test_monitor_temperature(mocker, event_loop):
    mock_temperature = 0

    class AsyncReadTemp(MagicMock):
        async def __call__(self, *args, **kwargs):
            super(AsyncReadTemp, self).__call__(*args, **kwargs)
            return mock_temperature

    mock_serial_number = "foo"
    mock_serial_number_bytes = bytes(mock_serial_number, "UTF-8")
    mock_device_file = f"{mock_serial_number}/bar"
    mocker.patch.object(ds18b20_dts.itertools, 'repeat', return_value=range(1))
    mocker.patch.object(ds18b20_dts, 'read_temp', new_callable=AsyncReadTemp)
    mocker.patch.object(ds18b20_dts, 'send_temperature', new_callable=AsyncMock)
    mocker.patch.object(ds18b20_dts, 'timer', return_value=0)
    mocker.patch.object(ds18b20_dts.asyncio, 'sleep', new_callable=AsyncMock)

    dts = object()
    await ds18b20_dts.monitor_temperature(dts, device_file=mock_device_file, loop=event_loop)

    # noinspection PyUnresolvedReferences
    ds18b20_dts.read_temp.assert_called_once_with(mock_device_file)
    # noinspection PyUnresolvedReferences
    ds18b20_dts.send_temperature.assert_called_once_with(dts, mock_serial_number_bytes, mock_temperature)


@pytest.mark.asyncio
async def test_monitor_humidity_twice(mocker, event_loop):
    count = 2
    mock_temperature = 0

    class AsyncReadTemp(MagicMock):
        async def __call__(self, *args, **kwargs):
            super(AsyncReadTemp, self).__call__(*args, **kwargs)
            return mock_temperature

    mock_serial_number = "foo"
    mock_device_file = f"{mock_serial_number}/bar"
    mocker.patch.object(ds18b20_dts.itertools, 'repeat', return_value=range(count))
    mocker.patch.object(ds18b20_dts, 'read_temp', new_callable=AsyncReadTemp)
    mocker.patch.object(ds18b20_dts, 'send_temperature', new_callable=AsyncMock)
    mocker.patch.object(ds18b20_dts, 'timer', return_value=0)
    mocker.patch.object(ds18b20_dts.asyncio, 'sleep', new_callable=AsyncMock)

    await ds18b20_dts.monitor_temperature(None, device_file=mock_device_file, loop=event_loop)

    # noinspection PyUnresolvedReferences
    assert ds18b20_dts.read_temp.call_count == count
    # noinspection PyUnresolvedReferences
    assert ds18b20_dts.send_temperature.call_count == count
    # noinspection PyUnresolvedReferences
    assert ds18b20_dts.asyncio.sleep.call_count == count


@pytest.mark.asyncio
async def test_monitor_humidity_ten(mocker, event_loop):
    count = 10
    mock_temperature = 0

    class AsyncReadTemp(MagicMock):
        async def __call__(self, *args, **kwargs):
            super(AsyncReadTemp, self).__call__(*args, **kwargs)
            return mock_temperature

    mock_serial_number = "foo"
    mock_device_file = f"{mock_serial_number}/bar"
    mocker.patch.object(ds18b20_dts.itertools, 'repeat', return_value=range(count))
    mocker.patch.object(ds18b20_dts, 'read_temp', new_callable=AsyncReadTemp)
    mocker.patch.object(ds18b20_dts, 'send_temperature', new_callable=AsyncMock)
    mocker.patch.object(ds18b20_dts, 'timer', return_value=0)
    mocker.patch.object(ds18b20_dts.asyncio, 'sleep', new_callable=AsyncMock)

    await ds18b20_dts.monitor_temperature(None, device_file=mock_device_file, loop=event_loop)

    # noinspection PyUnresolvedReferences
    assert ds18b20_dts.read_temp.call_count == count
    # noinspection PyUnresolvedReferences
    assert ds18b20_dts.send_temperature.call_count == count
    # noinspection PyUnresolvedReferences
    assert ds18b20_dts.asyncio.sleep.call_count == count


@pytest.mark.asyncio
async def test_monitor_humidity_sleep(mocker, event_loop):
    mock_temperature = 0

    class AsyncReadTemp(MagicMock):
        async def __call__(self, *args, **kwargs):
            super(AsyncReadTemp, self).__call__(*args, **kwargs)
            return mock_temperature

    mock_serial_number = "foo"
    mock_device_file = f"{mock_serial_number}/bar"
    mocker.patch.object(ds18b20_dts.itertools, 'repeat', return_value=range(1))
    mocker.patch.object(ds18b20_dts, 'read_temp', new_callable=AsyncReadTemp)
    mocker.patch.object(ds18b20_dts, 'send_temperature', new_callable=AsyncMock)
    mocker.patch.object(ds18b20_dts, 'timer', return_value=0)
    mocker.patch.object(ds18b20_dts.asyncio, 'sleep', new_callable=AsyncMock)

    await ds18b20_dts.monitor_temperature(None, device_file=mock_device_file, loop=event_loop)

    # noinspection PyUnresolvedReferences
    ds18b20_dts.asyncio.sleep.assert_called_once_with(30, loop=event_loop)


@pytest.mark.asyncio
async def test_run(mocker, event_loop):
    device_folders = ("foo", "bar")
    device_files = [f"{device_folder}/w1_slave" for device_folder in device_folders]
    dts_register_pathway_stub = mocker.stub(name='dts_register_pathway_stub')

    class DtsStub:
        def __init__(self):
            pass

        async def register_pathway(self, _):
            dts_register_pathway_stub(_)
            return None

    dts = DtsStub()
    mocker.patch.object(ds18b20_dts, 'read_rom', return_value="foo")
    mocker.patch.object(ds18b20_dts, 'monitor_temperature', new_callable=AsyncMock)
    mocker.patch.object(ds18b20_dts.os, 'system')
    mocker.patch.object(ds18b20_dts.glob, 'glob', return_value=device_folders)

    register_task, monitor_task = ds18b20_dts.run(dts=dts, loop=event_loop)
    await register_task
    await monitor_task

    assert ds18b20_dts.os.system.call_count == 2
    dts_register_pathway_stub.assert_called_once_with(ds18b20_dts.TemperatureEvent)
    # noinspection PyUnresolvedReferences
    assert ds18b20_dts.monitor_temperature.call_count == len(device_files)

    # noinspection PyUnresolvedReferences
    ds18b20_dts.monitor_temperature.assert_has_calls(
        tuple(unittest.mock.call(dts, device_file) for device_file in device_files),
        any_order=True)


@pytest.mark.asyncio
async def test_integration_dts(mocker, event_loop):
    mock_serial_number = "foo"
    mock_serial_number_bytes = bytes(mock_serial_number, "UTF-8")
    mock_temperature = 20.0
    mocker.patch.object(ds18b20_dts, 'current_timestamp_nanoseconds', return_value=0)

    test_queue = asyncio.Queue()

    async def handle_temperature(event, address, received_timestamp_nanoseconds):
        await test_queue.put(event)

    with DomainTypeSystem(loop=event_loop) as dts_receive:
        await asyncio.sleep(.1, loop=event_loop)
        await dts_receive.register_pathway(ds18b20_dts.TemperatureEvent)
        await dts_receive.handle_type(ds18b20_dts.TemperatureEvent,
                                      data_handlers=(
                                          handle_temperature,
                                      )
                                      )
        with DomainTypeSystem(loop=event_loop) as dts_send:
            await asyncio.sleep(.1, loop=event_loop)
            await dts_send.register_pathway(ds18b20_dts.TemperatureEvent)
            await ds18b20_dts.send_temperature(dts_send, mock_serial_number_bytes, mock_temperature)

            temperature_event = await test_queue.get()
    pytest.approx(mock_temperature, temperature_event.temperature)
    assert mock_serial_number_bytes == temperature_event.serial_number
    test_queue.task_done()
