@0xe2fcec547cc9c9eb;

using TimePointInNs = UInt64;
# Nanoseconds since the epoch.

struct TemperatureEvent {
    temperature @0 :Text;
    timestamp @1 :TimePointInNs;
    model @2 :Text;
    serialNumber @3 :Text;
}