import asyncio
from datetime import datetime, timezone
import glob
import itertools
import logging
import os
import signal
from timeit import default_timer as timer

import aiofiles
import capnpy
from domaintypesystem import DomainTypeSystem

TemperatureEvent = capnpy.load_schema('src.ds18b20_dts.schema.temperature_event', pyx=False).TemperatureEvent


def current_timestamp():
    # returns floating point timestamp in seconds
    return datetime.utcnow().replace(tzinfo=timezone.utc).timestamp()


def current_timestamp_nanoseconds():
    return current_timestamp() * 1e9


def read_rom(device_folder):
    name_file = device_folder + '/name'
    with open(name_file, 'r') as f:
        return f.readline()


async def read_device(device_file):
    async with aiofiles.open(device_file, 'r') as f:
        return (await f.read()).splitlines()


async def read_temp(device_file):
    lines = await read_device(device_file)
    # Analyze if the last 3 characters are 'YES'.
    while lines[0][-3:] != 'YES':
        lines = await read_device(device_file)
    # Find the index of 't=' in a string.
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        # Read the temperature
        temp_string = lines[1][equals_pos + 2:]
        return str(int(temp_string) / 1e3)
    else:
        raise ValueError("Temperature not reported correctly")


async def send_temperature(dts, serial_number, temperature):
    await dts.send_struct(TemperatureEvent(temperature=bytes(str(temperature), 'UTF-8'),
                                           timestamp=int(current_timestamp_nanoseconds()),
                                           model=b'DS18B20',
                                           serial_number=serial_number))


async def monitor_temperature(dts, device_file, loop=None):
    if loop is None:  # pragma: no cover
        loop = asyncio.get_event_loop()

    serial_number = bytes(device_file.split('/')[-2], 'UTF-8')
    for _ in itertools.repeat(None):
        start_time = timer()
        temperature = await read_temp(device_file)
        await send_temperature(dts, serial_number, temperature)
        logging.info("device: DS18B20, serial_number: {0}, temperature: {1}°C"
                     .format(serial_number.decode("UTF-8"), temperature))
        await asyncio.sleep(30 - (timer() - start_time), loop=loop)


def run(dts=None, loop=None):
    logging.basicConfig(level=logging.DEBUG)

    if not loop:  # pragma: no cover
        loop = asyncio.new_event_loop()

    if not dts:  # pragma: no cover
        dts = DomainTypeSystem(loop=loop)

    # These two lines mount the device:
    os.system('modprobe w1-gpio')
    os.system('modprobe w1-therm')

    base_dir = '/sys/bus/w1/devices/'

    device_folders = tuple(glob.glob(base_dir + '28*'))
    device_files = (device_folder + '/w1_slave' for device_folder in device_folders)

    for device_folder in device_folders:
        logging.debug('rom: {}'.format(read_rom(device_folder)))

    register_coro = dts.register_pathway(TemperatureEvent)
    monitor_coros = tuple(monitor_temperature(dts, device_file) for device_file in device_files)

    if not loop.is_running():  # pragma: no cover
        loop.run_until_complete(register_coro)
        asyncio.wait(monitor_coros, loop=loop)
        try:
            signal.signal(signal.SIGINT, signal.default_int_handler)
            loop.run_forever()
        except KeyboardInterrupt:
            pass
        finally:
            loop.close()
    else:
        return loop.create_task(register_coro), asyncio.wait(monitor_coros, loop=loop)

    if not dts:  # pragma: no cover
        dts.close()
